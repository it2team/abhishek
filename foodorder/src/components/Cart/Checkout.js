import React, { useState, useRef } from "react";
import classes from "./Checkout.module.css";

const isEmpty = (value) => value.trim() === "";
// const isNotNumber = (value) =>
//   value.isNaN() ? "Please Enter Numeric Value." : "";

const Checkout = (props) => {
  const [isFormInputValid, setIsFormInputValid] = useState({
    name: true,
    address: true,
    contact: true,
    pincode: true,
  });
  const nameInputRef = useRef();
  const addressInputRef = useRef();
  const contactInputRef = useRef();
  const pincodeInputRef = useRef();

  const confirmHandler = (event) => {
    event.preventDefault();

    const enteredName = nameInputRef.current.value;
    const enteredAddress = addressInputRef.current.value;
    const enteredContact = contactInputRef.current.value;
    const enteredPincode = pincodeInputRef.current.value;

    const enteredNameIsValid = !isEmpty(enteredName);
    const enteredAddressIsValid = !isEmpty(enteredAddress);
    const enteredContactIsValid =
      !isEmpty(enteredContact);
    const enteredPincodeIsValid =
      !isEmpty(enteredPincode);

    const isFormValid =
      enteredNameIsValid &&
      enteredAddressIsValid &&
      enteredContactIsValid &&
      enteredPincodeIsValid;

    setIsFormInputValid({
      name: enteredNameIsValid,
      address: enteredAddressIsValid,
      contact: enteredContactIsValid,
      pincode: enteredPincodeIsValid,
    });
    
    if(!isFormValid){
      return;
    }

    props.onConfirm({
      name: enteredName,
      address: enteredAddress,
      contact: enteredContact,
      pincode: enteredPincode,
      
    });
    

  };
  const namecltclass = `${classes.control} ${
    isFormInputValid.name ? "" : classes.invalid
  }`;
  const addresscltclass = `${classes.control} ${
    isFormInputValid.address ? "" : classes.invalid
  }`;
  const contactcltclass = `${classes.control} ${
    isFormInputValid.contact ? "" : classes.invalid
  }`;
  const pincodecltclass = `${classes.control} ${
    isFormInputValid.pincode ? "" : classes.invalid
  }`;

  return (
    <form className={classes.form} onSubmit={confirmHandler}>
      <div className={namecltclass}>
        <label htmlFor="name">Full Name :- </label>
        <input
          type="text"
          id="name"
          placeholder="Enter Name"
          ref={nameInputRef}
        />
        {!isFormInputValid.name && <p>Please Enter Valid Name!</p>}
      </div>
      <div className={addresscltclass}>
        <label htmlFor="address">Address :- </label>
        <input
          type="text"
          id="address"
          placeholder="Enter Address"
          ref={addressInputRef}
        />
        {!isFormInputValid.address && <p>Please Enter Valid Address!</p>}
      </div>
      <div className={contactcltclass}>
        <label htmlFor="contact">Contact :- </label>
        <input
          type="number"
          id="contact"
          placeholder="Number"
          ref={contactInputRef}
        />
        {!isFormInputValid.contact && (
          <p>Please Enter Valid Contact Details!</p>
        )}
      </div>
      <div className={pincodecltclass}>
        <label htmlFor="pincode">Pincode :- </label>
        <input
          type="text"
          id="pincode"
          placeholder="Pincode"
          ref={pincodeInputRef}
        />
        {!isFormInputValid.pincode && <p>Please Enter Valid Pincode!</p>}
      </div>
      <div className={classes.actions}>
        <button type="button" onClick={props.onCancel}>
          Cancel
        </button>
        <button className={classes.submit}>Submit</button>
      </div>
    </form>
  );
};

export default Checkout;
