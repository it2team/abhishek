import {
  Button,
  FormControl,
  FormGroup,
  Input,
  InputLabel,
  TextField,
  Typography,
} from "@mui/material";
import { makeStyles } from "@mui/styles";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { addUserData } from "../services/Api";

const useStyle = makeStyles({
  container: {
    width: "50%",
    margin: "10% 0 0 25%",
    "& > *": {
      marginTop: 20,
    },
  },
});

const initialValue = {
  name: "",
  username: "",
  email: "",
  phone: "",
};

const AddUser = () => {
  const classes = useStyle();
  const [user, setUser] = useState(initialValue);
  let navigate = useNavigate();

  const { username, name, email, phone } = user;

  const onValueChange = (event) => {
    //   console.log(event.target.value);
    setUser({ ...user, [event.target.name]: event.target.value });
  };

  //   const onChangeData = (event) => {
  //     onValueChange(event)
  //   };

  const addUserDetails = async () => {
    await addUserData(user);
    navigate("/all-user");
  };

  return (
    <FormGroup className={classes.container}>
      <Typography variant="h4">Add User</Typography>

      <FormControl style={{padding: '10px'}}>
        <TextField
          variant="standard"
          label="Name"
          onChange={(event) => onValueChange(event)}
          name="name"
          value={name}
          required
          
        />
      </FormControl>
      <FormControl style={{padding: '10px'}}>
        <TextField
          variant="standard"
          label="Username"
          onChange={(event) => onValueChange(event)}
          name="username"
          value={username}
          required
        />
      </FormControl>
      <FormControl style={{padding: '10px'}}>
        <TextField
          variant="standard"
          label="Email"
          onChange={(event) => onValueChange(event)}
          name="email"
          value={email}
          required
        />
      </FormControl>
      <FormControl style={{padding: '10px'}}>
        <TextField
          variant="standard"
          label="Phone"
          onChange={(event) => onValueChange(event)}
          name="phone"
          value={phone}
          required
        />
        <Button 
          variant="contained"
          color="primary"
          style={{ marginTop: "25px" }}
          onClick={() => addUserDetails()}
        >
          Submit
        </Button>
      </FormControl>
    </FormGroup>
  );
};

export default AddUser;
