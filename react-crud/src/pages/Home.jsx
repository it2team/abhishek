import React from 'react'

const Home = () => {
    return (
        <div>
            <h1 style={{textAlign: 'center' }}>Home Page</h1>
            <h1 style={{textAlign: 'center' , color:'#4CAF50'}}>Welcome To Abhishek Kadiwala World's</h1>
            <h1 style={{background:'#7B1FA2' , marginTop: '50vh', textAlign: 'center' , fontSize:'50px'}}>Created by <span style={{color:"white"}}>Abhishek Kadiwala</span> </h1>
            
        </div>
    )
}

export default Home
