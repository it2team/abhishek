import { makeStyles } from '@mui/styles';
import { Box } from '@mui/system';
import React from 'react';
import PageNotFound from '../assets/images/pagenotfound.png';


// const useStyles = makeStyles({
//     image:{
//         width: 50,
//         height: 50,
//         alignItems: 'center'
//     },
// })

const NotFound = () => {

    // const classes = useStyles();
    return (
        
        <img src={PageNotFound} style={{width: '30%' , margin: '80px 0 0 50px'}}/>
        
    )
}

export default NotFound;
