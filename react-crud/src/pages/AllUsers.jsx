import {
    Button,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
} from "@mui/material";
import { makeStyles } from "@mui/styles";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import Api, { deleteUserData } from "../services/Api";

const useStyle = makeStyles({
  thead: {
      
    "& > *": {
      backgroud: "#000000",
    }
  },
  table: {
      alignItems: 'center',
    width: "60%",
    border: "2px solid black",
    margin: "2% 2% 0 2%",
    font: 'Bold',
    padding: '14px'
  },
  tablepadding:{
    padding: '7px',
    "& > *": {
        backgroud: "#000000",
      }
  }
});
const AllUsers = () => {
  const [users, setUsers] = useState([]);
   let navigate =  useNavigate()
  const classes = useStyle();

  const allUserData = async () => {
    const response = await Api();
    // console.log(response);
    setUsers(response.data);
  };

  const deleteUser = async(id) =>{
    await deleteUserData(id);

    Api();
    var newRecord = users.filter((user) => {
      return user.id!==id;
    } )
    setUsers(newRecord);
  }

  useEffect(() => {
    allUserData();
  }, []);

  // useEffect(() => {
  //   deleteUser();
  // }, [deleteUserData()]);


  return (
    <Table className={classes.table}>
      <TableHead className={classes.table}>
        <TableRow className={classes.tablepadding}>
          <TableCell>id</TableCell>
          <TableCell>Name</TableCell>
          <TableCell>UserName</TableCell>
          <TableCell>Email</TableCell>
          <TableCell>Phone</TableCell>
          <TableCell></TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {users.map((user, index) => (
          <TableRow key={index}>
            <TableCell> {user.id}</TableCell>
            <TableCell> {user.name}</TableCell>
            <TableCell> {user.username}</TableCell>
            <TableCell> {user.email}</TableCell>
            <TableCell> {user.phone}</TableCell>
            <TableCell>
                <Button variant="contained" color="primary" style={{marginRight:'10px'}} onClick={() => navigate(`/edit/${user.id}`)}>Edit</Button>
                <Button variant="contained" color="secondary" onClick={() => {deleteUser(user.id)}}>Delete</Button>
            </TableCell>
          </TableRow>
        ))}
      </TableBody>
    </Table>
  );
};

export default AllUsers;
