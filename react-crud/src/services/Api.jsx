import axios from 'axios';

const apiUrl = "http://localhost:3005/users";

const Api = async (id) => {
    id = id || '';
    return await axios.get(`${apiUrl}/${id}`);

}

export const addUserData = async(user) => {
    return await axios.post(apiUrl , user);
} 

export const editUserData = async(id, user) => {
    return await axios.put(`${apiUrl}/${id}` , user);
}

export const deleteUserData = async(id) =>{
    return await axios.delete(`${apiUrl}/${id}`);

}
export default Api;

