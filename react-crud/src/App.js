import "./App.css";
import { Route, Routes } from "react-router-dom";
import NotFound from "./pages/NotFound";
import Home from "./pages/Home";
import AddUser from "./pages/AddUser";
import AllUsers from "./pages/AllUsers";
import NavBar from "./components/NavBar";
import EditUser from "./components/EditUser";

function App() {
  return (
    <>
    <NavBar />
    <Routes>
      <Route path='' element={<Home />} />
      <Route path='/' element={<Home />} />
      <Route exact path='add' element={<AddUser />} />
      <Route exact path='all-user' element={<AllUsers />} />
      <Route exact path='edit/:id' element={<EditUser />} />
      <Route  path='*' element={<NotFound />} />
    </Routes>
    </>
  );
}

export default App;
