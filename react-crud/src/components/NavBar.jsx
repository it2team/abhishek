import { AppBar, Toolbar } from '@mui/material';
import { makeStyles } from '@mui/styles';
import React from 'react';
import { NavLink } from 'react-router-dom';


const myStyle = makeStyles({
    header:{
        background: '#111111'
    },
    navlink:{
        color: 'white',
        textDecoration: 'none',
        marginRight: 20
    }
})

const NavBar = () => {

    const classes = myStyle();
    return (
        <AppBar className={classes.header} position="static" >
        <Toolbar>
            <NavLink className={classes.navlink} to='/'>Home</NavLink>
            <NavLink className={classes.navlink} to='all-user'>All User</NavLink>
            <NavLink className={classes.navlink} to='add'>Add User</NavLink>
        </Toolbar>
        </AppBar>
    )
}

export default NavBar;
