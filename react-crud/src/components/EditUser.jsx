import {
  Button,
  FormControl,
  FormGroup,
  Input,
  InputLabel,
  TextField,
  Typography,
} from "@mui/material";
import { makeStyles } from "@mui/styles";
import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import Api, { editUserData } from "../services/Api";

const useStyle = makeStyles({
  container: {
    width: "50%",
    margin: "10% 0 0 25%",
    "& > *": {
      marginTop: 20,
    },
  },
});

const initialValue = {
  name: "",
  username: "",
  email: "",
  phone: "",
};

const EditUser = () => {
  const classes = useStyle();
  const [user, setUser] = useState(initialValue);
  const { id } = useParams();
  let navigate = useNavigate();

  const { username, name, email, phone } = user;

  const loadUserData = async () => {
    const response = await Api(id);
    setUser(response.data);
  };

  useEffect(() => {
    loadUserData();
  }, []);

  const onValueChange = (event) => {
    //   console.log(event.target.value);
    setUser({ ...user, [event.target.name]: event.target.value });
  };

  const editUserDetails = async () => {
    await editUserData(id, user);
    navigate('/all-user')
  };

  return (
    <FormGroup className={classes.container}>
      <Typography variant="h4">Edit User</Typography>

      <FormControl style={{ padding: "10px" }}>
        <TextField
          variant="standard"
          label="Name"
          onChange={(event) => onValueChange(event)}
          name="name"
          value={name}
        />
      </FormControl>
      <FormControl style={{ padding: "10px" }}>
        <TextField
          variant="standard"
          label="Username"
          onChange={(event) => onValueChange(event)}
          name="username"
          value={username}
        />
      </FormControl>
      <FormControl style={{ padding: "10px" }}>
        <TextField
          variant="standard"
          label="Email"
          onChange={(event) => onValueChange(event)}
          name="email"
          value={email}
        />
      </FormControl>
      <FormControl style={{ padding: "10px" }}>
        <TextField
          variant="standard"
          label="Phone"
          onChange={(event) => onValueChange(event)}
          name="phone"
          value={phone}
        />
        <Button
          variant="contained"
          color="primary"
          style={{ marginTop: "25px" }}
          onClick={() => {editUserDetails();

          }}
        >
          Update Data
        </Button>
      </FormControl>
    </FormGroup>
  );
};

export default EditUser;
