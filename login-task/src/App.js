// import LoginAPI from "./api/LoginAPI";
import "./App.css";
import AllRoutes from './components/AllRoutes';

function App() {
  return (
    <div className="App">
      <AllRoutes />
      {/* <LoginAPI /> */}
    </div>
  );
}

export default App;
