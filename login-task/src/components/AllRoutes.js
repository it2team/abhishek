// All the Page Route Will be provide here

import React, { createContext, useState } from "react";
import { Route, Routes } from "react-router";
// import Homeconfig from "../config/Homeconfig";
// import Loginconfig from "../config/Loginconfig";
// import PageNotFoundconfig from "../config/PageNotFoundconfig";
import Home from "../pages/Home";
import Login from "../pages/Login";
import PageNotFound from "../pages/PageNotFound";
import Registration from "../pages/Registration";
// import ConditionalRoute from "./ConditionalRoute";
import RequiredAuth from "./RequiredAuth";

// export const UserContext = createContext();

const AllRoutes = () => {
  const [user, setUser] = useState('');
  // const [user, setUser] = useState({ isLoggedIn: true });

  return (
    // <UserContext.Provider value={{ user, setUser }}>
      <Routes>
        {user && (
          <>
            <Route path="/" element={<Home Login = {() => setUser(true)}/>} />
            <Route path="/home" element={<Home />} />
          </>
        )}
        {!user && <Route path="/signin" element={<Login />} />}

        <Route path="/signup" element={<Registration />} />
        {user && <Route path="*" element={<PageNotFound />} />}
      </Routes>

    // </UserContext.Provider> 
    // <div>
    //   <Homeconfig />
    //   <Loginconfig />
    //   <PageNotFoundconfig />
    // </div>
  );
};

export default AllRoutes;
