// Fields
// 1 - Name
// 2 - Email
// 3 - Password

import {
  Button,
  FormControl,
  FormGroup,
  TextField,
  Typography,
} from "@mui/material";
import React from "react";

const Registration = () => {
  return (
    <FormGroup>
      <Typography variant="h3" style={{ marginTop: "40px" }}>Sign Up</Typography>
      <FormControl>
        <TextField
          type="text"
          variant="outlined"
          label="Name"
          style={{ marginTop: "30px" }}
        />
      </FormControl>
      <FormControl>
        <TextField
          type="text"
          variant="outlined"
          label="E-mail"
          style={{ marginTop: "10px" }}
        />
      </FormControl>
      <FormControl>
        <TextField
          type="text"
          variant="outlined"
          label="Password"
          style={{ marginTop: "10px" }}
        />
      </FormControl>
      <FormControl>
        <Button
          variant="contained"
          color="primary"
          style={{ marginTop: "10px" }}
        >
          Sign Up
        </Button>
      </FormControl>
    </FormGroup>
  );
};

export default Registration;
