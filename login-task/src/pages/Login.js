import {
  Button,
  FormControl,
  FormGroup,
  TextField,
  Typography,
} from "@mui/material";
import React, { useContext, useRef, useState } from "react";
import axios from "axios";
import { useLocation, useNavigate } from "react-router";
import { UserContext } from "../components/AllRoutes";

const Login = () => {
  // const { user, setUser } = useContext(UserContext);
  const location = useLocation();
  const emailRef = useRef("abhi@test.com");
  const passwordRef = useRef("123");

  console.log("Default Email", emailRef);
  console.log("Default Password", passwordRef);

  const navigate = useNavigate();

  const [errorMess, setErrorMess] = useState("");

  const loginHandler = (e) => {
    // e.preventDefault();
    // setAuth({});   // setAuth Here
    console.log("button works");
    const enteredEmailRef = emailRef.current.value;
    const enteredPasswordRef = passwordRef.current.value;

    var errorMessage = "global";

    console.log(enteredEmailRef);
    console.log(enteredPasswordRef);

    const enteredValue = {
      email: enteredEmailRef,
      password: enteredPasswordRef,
    };
    // const db = new FormData();
    console.log(enteredValue);

    // Without async - await
    axios
      .post(
        "http://192.168.0.109:3030/customerLogin",
        enteredValue
        // {
        //   headers: {
        //     "Content-Type": "application/json",
        //     Accept: "application/json",
        //   },
        //   body: enteredValue,
        // }
      )
      .then((response) => {
        console.log(response);
        navigate("/home");
        // navigate("/home")
      })
      .catch((error) => {
        console.log("Data Error", error);

        errorMessage = error.response.data; // Declare Above
        setErrorMess(JSON.stringify(errorMessage.message)); // Convert Object To string
        // setErrorMess(errorMessage);
        console.log(JSON.stringify(errorMessage));

        // console.log(error.response.data);
        // return (<h3>{error.response.data}</h3>)
        // alert("Authentication Fail Please Enter Correct Details", error);
      });

    

    // console.log("Globale Scope", errorMessage);
    // const data = response.message;
    // console.log("response After Data Submit", response);

    // With async - await
    // const response = async () =>
    //   await axios.post(
    //     "http://192.168.0.109:3030/customerLogin",
    //     enteredValue
    //   );
    // const data = response.message;
    // console.log("response After Data Submit", response);

    // const responseMessageHandler = () => {
    //   if (response.ok) {
    //     console.log("Login Success....");
    //   } else {
    //     console.log("Login Fail");
    //   }
    // };
  };
  // const afterLoginHandler = () => {
  //   if (user.isLoggedIn) return;
  //   setUser({ isLoggedIn: true });

  //   if (location.state?.from) {
  //     navigate(location.state.from);
  //   }
  // };

  return (
    <>
      <FormGroup>
        <Typography variant="h3" style={{ marginTop: "40px" }}>
          Sign In
        </Typography>
        <FormControl>
          <FormControl>
            <TextField
              type="text"
              variant="outlined"
              label="E-mail"
              inputRef={emailRef}
              style={{ marginTop: "30px" }}
            />
          </FormControl>
          <FormControl>
            <TextField
              type="text"
              variant="outlined"
              label="Password"
              inputRef={passwordRef}
              style={{ marginTop: "10px" }}
            />
          </FormControl>
          <div style={{ color: "red", marginTop: "10px" }}>{errorMess}</div>
          <FormControl>
            <Button
              variant="contained"
              color="primary"
              style={{ marginTop: "20px" }}
              // onClick = { () => loginHandler()}
              onClick = {() => {
                loginHandler();
                // afterLoginHandler();
              }}
            >
              Sign In
            </Button>
          </FormControl>
          {/* <FormControl component='h2'> */}
          {/* {console.log(errorMess)} */}
          {/* {errorMess} */}

          {/* </FormControl> */}
        </FormControl>
        <FormControl style={{ marginTop: "20px" }}>
          {/* <Input
            value={errorMess}
            style={{ marginTop: "5px", color: "red" , alignContent:'center' }}
            disableUnderline={true}
          /> */}
        </FormControl>
      </FormGroup>
    </>
  );
};

export default Login;
