import { Route, Routes, Navigate } from "react-router-dom";
import Comments from "./components/comments/Comments";
import Layout from "./components/layout/Layout";
import AllQuotes from "./pages/AllQuotes";
import FunWithText from "./pages/FunWithText";
import NewQuote from "./pages/NewQuote";
import NotFound from "./pages/NotFound";
import QuoteDetails from "./pages/QuoteDetails";

function App() {
  return (
    <div>
      <Layout>
        <Routes>
          <Route exact path="/" element={<AllQuotes />}></Route>
          {/* <Route exact path="/" element={<Navigate to="/quotes" />}></Route> */}
          <Route path="/quotes" element={<AllQuotes />}></Route>
          <Route path="/quotes/:quoteId" element={<QuoteDetails />}>
            <Route path="comments" element={<Comments />}></Route>
          </Route>
          <Route path="/new-quote" element={<NewQuote />}></Route>
          <Route
            path="/funwithtext/:funwithtextId"
            element={<FunWithText />}
          ></Route>
          <Route path="*" element={<NotFound />}></Route>
        </Routes>
      </Layout>
    </div>
  );
}

export default App;
