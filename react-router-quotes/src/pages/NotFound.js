import React from 'react';

const NotFound = () => {
    return (
        <div className='center'>
            <p>404 Page Not Found!</p>
        </div>
    )
}

export default NotFound;
