import "./App.css";
import React, { useState } from "react";
import ClickEvent from "./component/ClickEvent";
import PropsFunctional from "./component/PropsFunctional";
import ClassComponent from "./component/ClassComponent";
import FunctionalComponent from "./component/FunctionalComponent";
import UseStateClass from "./component/UseStateClass";
import UseStateFunctional from "./component/UseStateFunctional";
import PropsClass from "./component/PropsClass";
import Navbar from "./component/Navbar";
import Footer from "./component/Footer";
import InputBox from "./component/InputBox";
import Form from "./component/Form";

function App() {
  
    // const [name, setName] = useState("Test-1");
    // const [department, setDepartment] = useState("Computer");
  
  return (
    <>
      {/* <Navbar />
      <FunctionalComponent />
      <ClassComponent />
      <ClickEvent />
      <UseStateFunctional />
      <UseStateClass />
      <PropsFunctional name={name} department={department} />
      <center ><button  onClick={() => {setName("Test-2") ; setDepartment("IT")}} >
        Click Me
      </button>
      </center>
      <PropsClass />
      <InputBox /> */}
      <Form />
      

      {/* <Footer /> */}
    </>
  );
}

export default App;
