import React, { useState } from "react";

export default function Form() {
  const [name, setName] = useState("");
  const [age, setAge] = useState();
  const [birth, setBirth] = useState();
  const [tnc, setTnc] = useState(false);

  function GetFormData(event) {
    event.preventDefault();
    alert("Your Name:-"+ name + "\n" +"Your Age:-"+ age + "\n" +"Your DOB:-"+ birth+ "\n" + "Tnc:- " +tnc);
    // alert(age);
    // alert(birth);
    // alert(tnc);

  }
  return (
    <>
      <center>
        <h1 className="Center my-3">Hello Your Form Here</h1>
        <form onSubmit={GetFormData}>
          <table>
            <tr>
            <td><label className="Center">Enter Your Name:- </label>
            <input
              id="name"
              type="text"
              placeholder="Enter Your Name"
              onChange={(event) => setName(event.target.value)}
            />
            </td>
            </tr>
            <br></br> <br></br>
            <tr>
            <td>
            <label className="Center my-3">Enter Your Date of Birth:- </label>
            <input
              id="dob"
              type="date"
              placeholder="Enter Your Date Of Birth"
              required
              onChange={(event) => setBirth(event.target.value)}
            />
            </td>
            </tr>
            
            <br></br> <br></br>
            <tr>
            <td>
            <label className="Center ">Enter Your Age:- </label>
            <input
              id="age"
              type="number"
              min="1"
              placeholder="Enter Your Age"
              required
              onChange={(event) => setAge(event.target.value)}
            />
            </td>
            </tr>
            <br></br> <br></br>
            <tr>
            <input type="checkbox" onChange={(event) => setTnc(event.target.checked)} />
            <span> Accepts Term and Condition</span>
            <br></br> <br></br>
            </tr>
            <tr>
            <td>
            <button type="submit">
              Fetch Data
            </button>
            </td>
            </tr>
            <tr>
            <td>
            <button type="reset" >
              Clear Data
            </button>
            </td>
            </tr>
          </table>
        </form>
      </center>
    </>
  );
}
