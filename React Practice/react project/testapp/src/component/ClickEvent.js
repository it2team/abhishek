function ClickEvent() {
    let fname ="Abhishek";
    let lname = "Kadiwala";
  return (

    <>
      <center>
        <form>
            <label>First Name:- </label>
          <input
            type="text"
            id="fname"
            name="First Name"
            placeholder="Enter First Name"
          />
          <br />
          <br />
          <label>Last Name:- </label>
          <input
            type="text"
            id="lname"
            name="Last Name"
            placeholder="Enter Last Name"
          />
          <br />
          <br />
          <button
            type="submit"
            onClick={() => alert(`Your First Name is ${fname} & Last Name is ${lname} `)}
          >
            Click me
          </button>
        </form>
      </center>
    </>
  );
}

export default ClickEvent;
