import { useState } from "react";

function UseStateFunctional() {
  const [name, setName] = useState("Abhi");
  const [age, setAge] = useState(22);
  const [clickcount, setClickcount] = useState(0);

  function UpdateData() {
    setName("Abhishek");
    setAge(21);
    setClickcount(clickcount + 1);
  }

  return (
    <>
      <center>
        <h1>Name:- {name}</h1>
        <h1>Age:- {age}</h1>
        <h1>ClickCount:- {clickcount}</h1>
        <button onClick={UpdateData}>Click Me To Update Data</button> 
      </center>
    </>
  );
}

export default UseStateFunctional;
