import React, { useState } from "react";

export default function InputBox() {
  const [data, setData] = useState(null);
  const [print, setPrint] = useState(false);

  function UpdateData(val) {
    setData(val.target.value);
    setPrint(false);
  }
  return (
    <>
      <center>
          <h3 className="my-3"> Enter What you want</h3>
        {print ? <h3>{data}</h3> : null}
        <input type="text" onChange={UpdateData} />
        <br></br>
        <button className="my-3" onClick={() => setPrint(true)}>Click Me </button>
      </center>
    </>
  );
}
