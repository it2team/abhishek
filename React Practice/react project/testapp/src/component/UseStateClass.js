import React, { useState, Component } from "react";

class UseStateClass extends Component {
  constructor() {
    super();
    this.state = {
      name: "Test",
      age: 20,
      clickcount: 0,
    };
  }

  UpdateData() {
    this.setState({
      name: this.state.name,
      age: this.state.age + 1,
      clickcount: this.state.clickcount + 1,
    });
  }

  render() {
    return (
      <>
        <center>
          <h1>Name:- {this.state.name}</h1>
          <h1>Age:- {this.state.age}</h1>
          <h1>ClickCount:- {this.state.clickcount}</h1>
          <button onClick={() => this.UpdateData()}>
            Click Me To Update Data
          </button>
        </center>
      </>
    );
  }
}

export default UseStateClass;
