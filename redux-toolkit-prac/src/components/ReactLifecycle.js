import React, { useEffect, useState } from "react";

const ReactLifecycle = () => {
  const [count, setCount] = useState(0);

  // Initial DOM or ComponentdidMount
  useEffect(() => {
    console.log("This is Initial Dom or update Component after every event");
  });

  //ComponentdidMount
  useEffect(() => {
    if (count == 2) {
        console.warn("Count", count);
        console.log(
          "Update Component only Once when page is loaded or untill Dependency is not match"
      );
    }
  }, [count]);
  
  //ComponentWillUnmount
  useEffect(() => {
    if (count == 2) {
      console.warn("Count", count);
      console.log(
        "Update Component only Once when page is loaded or untill Dependency is not match"
      );

      return () => {
        console.log("Component Will Unmount");
      };
    }
  }, [count]);

  return (
    <div>
      <h1>This is React Life Cycle Page</h1>
      <h2>{count}</h2>
      <button onClick={() => setCount(count + 1)}>Test</button>
    </div>
  );
};

export default ReactLifecycle;
