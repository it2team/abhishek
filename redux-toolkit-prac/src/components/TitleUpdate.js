import React, { useEffect, useState } from "react";

const TitleUpdate = () => {

    const [title, setTitle] = useState('');

    const [name, setName] = useState("abhi");
    // console.log("Default Title:- ",document.title= title)

    useEffect(() => {
      dataUpdate();
    },[])
    
    const dataUpdate = (e) =>{

    
      setTitle(document.title = name);

        // console.log("Default Title:- ",setTitle(document.title))
        // setTitle();
        
    }

  return (
    <div>
      <div>
        <h1>This is working {title}</h1>
        <input value={name}  onChange={(e) => setName(e.target.value)}/>
        <button onClick={()=>dataUpdate()}>Update</button>
        {/* <button onClick={()=>dataUpdate()}>Update</button> */}
      </div>
    </div>
  );
};

export default TitleUpdate;

// ---------------------  Below Code is Old Code  -------------------------------------//

// import React, { useEffect, useState } from "react";

// const TitleUpdate = () => {
//     const [title, setTitle] = useState('demo');
//   // This effect runs once, after the first render

//   const updateTitle = () =>{
//     //   document.title = {title};
//       console.log("Default Title= ",title);
//   }
//   useEffect(() => {
//     updateTitle()
//   }, []);

//   return (
//     <>
//       <h1>This is working</h1>
//       <input placeholder="Enter Value" onClick={(e)=> setTitle(e.target.value)}></input>
//       <button onClick={() => updateTitle()} />
//     </>
//   );
// };

// export default TitleUpdate;
