// All the Data Fetching , Stucture and Destructure of Data using useSelector

// Practice of useSelector Hook which Provided by Redux

// Use of useSelector is for Fetch Data from store

import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { changeAge,changeName,changeAddress } from "../reducers/userReducer";

const Bio = () => {
  const { name, age, address, designation } = useSelector((state) => {
    console.log("This is State Data", state);
    return state;
  });

  // const [age, setAge] = useState();
  // const [name, setName] = useState();
  // const [address, setAddress] = useState();

  const dispatch = useDispatch();
  const updateAge = (age) => {
    // dispatch({ type: "Update_Age", payload: age });
    dispatch(changeAge(age))
  };
  const updateName = (name) => {
    // dispatch({ type: "Update_Name", payload: name });
    dispatch(changeName(name))
  };
  const updateAddress = (address) => {
    // dispatch({type:"Update_Address", payload: address})
    dispatch(changeAddress(address))
  }

  // const updateAllData = (age) =>{
  //   dispatch(changeAge(age))
  //   dispatch(changeName(name)),
  //   dispatch(changeAddress(address))
  // }
  
  return (
    <div>
      <h1>
        I am {name} from {address}
      </h1>
      <h1>My Age is {age}</h1>
      <h1>I am {designation}</h1>
      <button onClick={()=> updateAge(40)}>Update Age</button>
      <button onClick={() => updateName("Abhi")}>Update Name</button>
      <button onClick={() => updateAddress("U.S")}>Update Address</button>
      {/* <input name='age' onChange={(e)=> setAge(e.target.value)} >Update Age</input>
      <input name='age' onChange={(e)=> setName(e.target.value)}>Update Name</input>
      <input name='age' onChange={(e)=> setAddress(e.target.value)}>Update Address</input>
      <button onClick={updateAllData()} >Update All Data</button> */}
    </div>
  );
};

export default Bio;
