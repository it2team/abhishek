// import { createReducer } from "@reduxjs/toolkit";
import { createSlice } from "@reduxjs/toolkit";
import React from "react";
// import { updateAddress } from "./Actions";

const initialState = {
  name: "Abhishek",
  age: 20,
  address: "surat",
  designation: "React JS Developer",
};
{console.log("This is Initial State",initialState)}

// --------------   Write Code For createSlice below    ------------------ //

const userReducer = createSlice({
  name: "person",
  initialState,
  reducers: {
    changeAge(state, action) {
      state.age = action.payload;
    },
    changeName(state, action) {
      state.name = action.payload;
    },
    changeAddress(state, action) {
      state.address = action.payload;
    },
  },
});

export const {changeAge,changeName,changeAddress} = userReducer.actions
export default userReducer.reducer

// --------------   Below Code Is based on createReducer and createAction    ----------------- //

// const userReducer = createReducer(initialState, (update) => {
//     update.addCase("Update_Name", (state, action) => {
//     state.name = action.payload;
//   });
//   update.addCase("Update_Age", (state, action) => {
//     state.age = action.payload;
//   });
//   update.addCase(updateAddress, (state, action) => {
//     state.address = action.payload;
//   });
// });
// export default userReducer;

// --------------   Below Code Is based on Redux Pattern    ----------------- //

// const userReducer = (state = initialState, action) => {
//   if (action.type == "Update_Age") {
//     return {
//         ...state,
//       age: action.payload
//     };
//   }
//   return state;
// };

// export default userReducer;
