import './App.css';
import Bio from './components/Bio';
import ReactLifecycle from './components/ReactLifecycle';
import TitleUpdate from './components/TitleUpdate';

function App() {
  return (
    <div className="App">
      {/* <Bio />
      <TitleUpdate /> */}
      <ReactLifecycle />
    </div>
  );
}

export default App;
