import React, { useReducer } from "react";

const reducer = (state, action) => {
    console.log(state,action);
  if (action.type === "Increment") {
    return state + 1;
  }
  if (action.type === "Decrement") {
    return state - 1;
  }
  return state;
};

const ReducerPrac = () => {
  const [state, dispatch] = useReducer(reducer, 0);

  return (
    <>
      <center>
          <h1>Hello </h1>
        <h2>{state}</h2>
        <div>
          <button onClick={() => dispatch({ type: "Increment" })}>
            Increment
          </button>
          <button onClick={() => dispatch({ type: "Decrement" })}>
            Decrement
          </button>
        </div>
      </center>
    </>
  );
};

export default ReducerPrac;
