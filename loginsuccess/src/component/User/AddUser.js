import React, { useState } from "react";
import Card from "../UI/Card";
import classes from "./AddUser.module.css";
import Button from "../UI/Button";

const AddUser = (props) => {
  const [enteredUsername, setEnteredUsername] = useState();
  const [enteredUserAge, setEnteredUserAge] = useState();

  const addUserHandler = (event) => {
    event.preventDefault();
    if (
      enteredUsername.trim().length === 0 &&
      enteredUserAge.trim().length === 0
    ) {
      return;
    }
    if (+enteredUserAge < 1) {
      return;
    }
    props.onAddUser(enteredUsername, enteredUserAge);
    setEnteredUsername("");
    setEnteredUserAge("");
  };

  const UpdateUsername = (event) => {
    setEnteredUsername(event.target.value);
  };
  const UpdateUserAge = (event) => {
    setEnteredUserAge(event.target.value);
  };

  return (
    <Card className={classes.input}>
      <form onSubmit={addUserHandler}>
        <label htmlFor="username">Username</label>
        <input
          id="username"
          type="text"
          value={enteredUsername}
          onChange={UpdateUsername}
        />
        <br />
        <br />
        <label htmlFor="age">Age</label>
        <input
          id="age"
          type="Number"
          value={enteredUserAge}
          onChange={UpdateUserAge}
        />
        <br />
        <br />
        <Button type="submit">Click Me</Button>
      </form>
    </Card>
  );
};

export default AddUser;
