import React from "react";
import { PageOwner } from "./ContextApi";

function ComC() {
  return (
    <div>
      <h1 style={{backgroundColor: "skyblue"}}>This is Context Api Practice Page</h1>
      <h3 style={{ color: "rebeccapurple" }}>
        Direct Access Components Instead Of Passing every Level.
      </h3>
      <PageOwner.Consumer>
        {(Owner) => {
          return <h2>This Page Owner Is:- {Owner} </h2>;
        }}
      </PageOwner.Consumer>
    </div>
  );
}

export default ComC;
