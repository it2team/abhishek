import React, { createContext } from 'react'
import ComC from './ComC';

// Implementation of Context Api (Key Points)
// 1) createContext
// 2) Provider
// 3) Consumer

const PageOwner = createContext();

const ContextApi = () => {

    return (
        <div>
            <PageOwner.Provider value={"Abhishek Kadiwala"}>
                <ComC />
            </PageOwner.Provider> 
        </div>
    )
}

export default ContextApi;
export { PageOwner };