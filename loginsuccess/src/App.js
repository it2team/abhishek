import { useState } from "react";
import "./App.css";
import ContextApi from "./component/ContextAPI/ContextApi";
import AddUser from "./component/User/AddUser";
import ReducerPrac from "./component/User/ReducerPrac";
import UserList from "./component/User/UserList";
import Registration from "./Registration/Registration";

function App() {
  // const [userListData, setUserListData] = useState([]);
  // const AddUserHandler = (uName, uAge) => {
  //   setUserListData((prevUserList) => {
  //     return [
  //       ...prevUserList,
  //       { name: uName, age: uAge, id: Math.random().toString() },
  //     ];
  //   });
  // };
  return (
    <div className="App">
      {/* <h1>Login Page</h1>
      <AddUser onAddUser={AddUserHandler} />
      <UserList users={userListData} />
      <ReducerPrac />
      <ContextApi /> */}
      <Registration />

    </div>
  );
  
  
}

export default App;
