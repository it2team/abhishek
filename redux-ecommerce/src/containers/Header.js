import React from "react";

const Header = () => {
  return (
    <div className="ui sizer vertical segment">
      <div className="ui large header">
        <h1>Ecommerce</h1>
      </div>
    </div>
  );
};

export default Header;
