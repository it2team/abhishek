import React from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";

const ProductComponents = () => {
  const products = useSelector((state) => state.allProducts.products);

  const renderList = products.map((product) => {
    const { id, title, price, image, category } = product;
    return (
      <div className="ui link cards" key={id}>
        <Link to={`product/${id}`}>
        <div className="card" >
          <div className="image">
            <img src={image} />
          </div>
          <div className="content">
            <div className="header">{title}</div>
            <div className="meta">
              <span className="meta">$ {price}</span>
            </div>
            <div className="meta">{category}</div>
          </div>
        </div>
        </Link>
      </div>
    );
  });

  return <>{renderList}</>;
};

export default ProductComponents;
