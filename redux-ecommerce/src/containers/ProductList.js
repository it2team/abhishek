import React, { useEffect } from "react";
import axios from "axios";
import {setProducts} from '../redux/actions/productActions'; 
import { useDispatch, useSelector } from "react-redux";
import ProductComponents from "./ProductComponents";

const ProductList = () => {
//   const products = useSelector((state) => state);
  const dispatch = useDispatch();

  const productsdata = async () => {
    const response = await axios
      .get("https://fakestoreapi.com/products")
      .catch((error) => {
        console.log(error);
      });
    dispatch(setProducts(response.data));
  };

  useEffect(() => {
    productsdata();
  },[]);
//   console.log("Products:" , products);


  return (
    <div className="ui grid container">
      <ProductComponents />
    </div>
  );
};

export default ProductList;
