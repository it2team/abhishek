// Make Products Reducers Here
// every Reducer have state and actions
import actionTypes from "../contants/actionTypes";

const initialState = {
  products: [],
};
// const secondState = {
//     productInfo: {},
// }
export const productReducer = (state = initialState, actions) => {
  switch (actions.type) {
    case actionTypes.SET_PRODUCTS:
      return { ...state, products: actions.payload };
    default:
      return state;
  }
};

export const selectedProductReducer = (state = {}, actions) => {
  switch (actions.type) {
    case actionTypes.SELECTED_PRODUCT:
      return { ...state, ...actions.payload };
    case actionTypes.REMOVE_SELECTED_PRODUCT:
        return {};
    default:
      return state;
  }
};
