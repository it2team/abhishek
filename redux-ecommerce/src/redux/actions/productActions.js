//perform action of products here
import actionTypes  from "../contants/actionTypes";

// action of Set Products
export const setProducts = (products) => {
    return { 
        type: actionTypes.SET_PRODUCTS,
        payload: products };
    };
    

// action of Selected Products
export const selectedProducts = (product) => {
  return { 
    type: actionTypes.SELECTED_PRODUCT,
    payload: product };
};

// action of Remove Selected Products
export const removeSelectedProducts = () => {
    return { 
      type: actionTypes.REMOVE_SELECTED_PRODUCT
      };
  };