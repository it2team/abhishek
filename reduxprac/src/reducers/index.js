import changeNumberState from './upDown';

import { combineReducers} from "redux";

const rootReducer = combineReducers({
    changeNumberState
});

export default rootReducer;