import React from "react";

function Footer() {
  return (
    <div>
      <div className="footer-copyright text-center py-3">
        © 2022 Copyright:
        <a href="https://abhishekkadiwala.netlify.app/" target="_blank"> abhishekkadiwala.com</a>
      </div>
    </div>
  );
}

export default Footer;
