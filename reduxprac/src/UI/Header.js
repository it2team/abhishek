import React from 'react'
import Home from './Home';
import Login from './Login';
import Logout from './Logout';

function Header() {
    return (
        <div className="mt-5">
            <div className="">
            <h2>Redux App</h2>
            <Home />
            <Login />
            <Logout />

            </div>
        </div>
    )
}

export default Header;
