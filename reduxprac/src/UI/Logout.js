import React from 'react'

const Logout = () => {
    return (
        <div > 
            <a className="btn btn-info"> Logout</a>
        </div>
    )
}

export default Logout;
