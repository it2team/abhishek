import "./App.css";
import Footer from "./UI/Footer";
import Header from "./UI/Header";
import {useSelector ,useDispatch} from "react-redux";
import { Decrement, Increment } from "./actions/IncreDec";

function App() {

  const myState = useSelector((state) => state.changeNumberState);
  const dispatch = useDispatch();
  return (
    <div className="App">
      <>
        <Header />
        <h1>Increment || Decrement Counter Using Redux</h1>
        <div className="">
          <a className="auto px-5"  href="#" title='Decrement' onClick={() => dispatch(Decrement()) }>-</a>
          <input className="col-xs-2" type="text" value={myState} />
          <a className="auto px-5"  href="#" title='Increment' onClick={() => dispatch(Increment())}>+</a>
        </div>
        <Footer />
      </>
    </div>
  );
}

export default App;
