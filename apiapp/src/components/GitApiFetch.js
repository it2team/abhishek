import React, { useEffect, useState } from "react";

const GitApiFetch = () => {
  const [userData, setUserData] = useState([]);

  const getUser = async () => {
    const response = await fetch("https://api.github.com/users");
    setUserData(await response.json());
  };

  useEffect(() => {
    getUser();
  }, []);

  return (
    <div>
      {userData.map((user) => {
        return (
          <>
            <table>
              <tr>
                <thead>
                  <h3>{user.id}. </h3>
                </thead>

                <td>
                  <img
                    style={{ maxBlockSize: "100px" }}
                    src={user.avatar_url}
                  />
                </td>

                <tbody>
                  <h3>{user.login}</h3>
                </tbody>
                
                              
              </tr>
            </table>
          </>
        );
      })}
    </div>
  );
};

export default GitApiFetch;
