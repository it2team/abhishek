import React, { useEffect, useState } from "react";
import axios from "axios";

const GitApiAxios = () => {
  const [userData, setUserData] = useState([]);

  const userInfo = () => {
    axios
      .get("https://jsonplaceholder.typicode.com/posts")
      .then((res) => {
        console.log(res);
        setUserData(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  useEffect(() => {
    userInfo();
  }, []);

  return (
    <div>
      <ul>
        {userData.map((user) => (
          <li key={user.id}>title:- {user.title}</li>
        ))}
      </ul>
    </div>
  );
};

export default GitApiAxios;
