import './App.css';
import GitApiAxios from './components/GitApiAxios';
import GitApiFetch from './components/GitApiFetch';

function App() {
  return (
    <div className="">
      <h1 style={{textAlign: "center"}}>GitHub Users Dummy List</h1>
      {/* <GitApiFetch /> */}
      <GitApiAxios />
    </div>
  );
}

export default App;
